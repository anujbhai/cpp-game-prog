/*
 * main.cpp
 * Print multiple lines
 * Anuj Upadhyay
 */
#include <cstdio>

int main()
{
    // greetings
    printf("Hello, my name is Anuj.");
    // location description
    printf("\nI live in Tezpur.");
    // hobby description
    printf("\nI like to play the guitar.");
}
